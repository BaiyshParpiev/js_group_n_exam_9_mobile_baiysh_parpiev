import React, {useEffect} from 'react';
import PlaceItem from "../../components/PlaceItem/PlaceItem";
import {ScrollView, StyleSheet, View, Text, Button} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {fetchContacts, open,} from "../../actions";
import ModalShow from "../../components/Modal/ModalShow";

const Contact = () => {
    const dispatch = useDispatch();
    const contact = useSelector(state => state.contact);

    const modalShow = () => {
        dispatch(open())
    }
    console.log(contact)
    useEffect(() => {
        dispatch(fetchContacts())
    }, [dispatch])



    return (
        <View style={styles.container}>
            <View style={styles.controls}>
                <Text style={styles.pizza}>Contacts</Text>
            </View>
            <ScrollView style={styles.placesList}>
                {contact && Object.keys(contact).map((p, i) => (
                    <View  key={i}>
                        <PlaceItem onPress={modalShow} placeName={contact[p]}/>
                        <ModalShow info={contact[p]}/>
                    </View>
                ))}
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    placesList: {
        width: '100%',
        marginTop: 20,
        marginRight: 0,
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 20,
        paddingTop: 40,
    },
    pizza: {
        flex: 1,
        textAlign: 'center',
        fontSize: 25,
        borderBottomWidth: 3,
        borderBottomColor: 'blue',
        borderBottomRightRadius: 2,
        marginRight: 10,
    },
    controls: {
        flexDirection: 'row',
        padding: 20,
    },
});


export default Contact;