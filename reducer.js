import {
    ADD, CANCEL, CLOSE,
    FETCH_CONTACT_FAILURE, FETCH_CONTACT_REQUEST, FETCH_CONTACT_SUCCESS, OPEN,
} from "./actions";

const initialState = {
    contact: null,
    loading: false,
    fetchLoading: false,
    error: null,
    modal: false,
}

export const reducer = (state = initialState, action) => {
    switch (action.type){
        case ADD:
            return {...state, price: state.price + action.payload};
        case CLOSE:
            return {...state, modal: false};
        case OPEN:
            return {...state, modal: true};
        case CANCEL:
            return {...state, orders: null}
        case FETCH_CONTACT_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_CONTACT_SUCCESS:
            return {...state, fetchLoading: false, contact: action.payload};
        case FETCH_CONTACT_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        default:
            return state;
    }
}