import React from "react";
import {Alert, Modal, StyleSheet, Text, Pressable, View, Button, Image} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {cancel, close} from "../../actions";

const ModalShow = ({info}) => {
    const dispatch = useDispatch();
    const modalVisible = useSelector(state => state.modal);

    const onCancel = () => {
        dispatch(cancel())
        dispatch(close())
    }
    return (
        <View style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    dispatch(close());
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <View style={styles.container}>
                            <Image
                                style={styles.stretch}
                                source={{uri: info.img}}
                            />
                        </View>
                        <View style={styles.modalText}><Text>Name: {info.name}</Text></View>
                        <View style={styles.modalText}><Text>Email: {info.email}</Text></View>
                        <View style={styles.modalText}><Text>Phone: {info.phone}</Text></View>
                        <Button style={styles.buttonOpen} onPress={onCancel} title="Back to List"/>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
        height: '100%',
        width: '100%'
    },
    img:{
        width: 50,
        height: 40,
    },
    modalView: {
        margin: 20,
        width: '80%',
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "black",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    stretch: {
        marginBottom: 30,
        width: 50,
        height: 40,
    }
});

export default ModalShow;