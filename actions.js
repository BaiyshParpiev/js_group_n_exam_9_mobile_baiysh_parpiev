import {axiosApi} from "./axiosApi";

export const FETCH_CONTACT_REQUEST = 'FETCH_CONTACT_REQUEST';
export const FETCH_CONTACT_SUCCESS = 'FETCH_CONTACT_SUCCESS';
export const FETCH_CONTACT_FAILURE = 'FETCH_CONTACT_FAILURE';
export const ADD = 'ADD';
export const CLOSE = 'CLOSE';
export const OPEN = 'OPEN';
export const CANCEL = 'CANCEL'

export const add = num => ({type: ADD, payload: num});
export const close = () => ({type: CLOSE});
export const open = () => ({type: OPEN});
export const cancel = () => ({type: CANCEL})

export const fetchContactRequest = () => ({type: FETCH_CONTACT_REQUEST});
export const fetchContactSuccess = contact => ({type: FETCH_CONTACT_SUCCESS, payload: contact});
export const fetchContactFailure = error => ({type: FETCH_CONTACT_FAILURE, payload: error});

export const fetchContacts = () => {
    return async dispatch => {
        try{
            dispatch(fetchContactRequest());
            const response = await axiosApi.get('/contact.json');
            dispatch(fetchContactSuccess(response.data));
        }catch(error) {
            dispatch(fetchContactFailure(error));
            throw error;
        }
    }
}




