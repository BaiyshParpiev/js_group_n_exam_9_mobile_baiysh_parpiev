import React from 'react';
import {reducer} from "./reducer";
import {applyMiddleware, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import Contact from "./containers/Contact/Contact";


export default function App() {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));
  return (
      <Provider store={store}>
        <Contact/>
      </Provider>
  );
}

